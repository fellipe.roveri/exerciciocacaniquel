package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class CacaNiquel {
    Output output = new Output();
    private int qtdSlots = 3;
    private ArrayList<Slots> sorteados = new ArrayList<>();
    int soma = 0;

    public void executarCacaNiquel (){
        sortear();
        boolean validaTresIguais = validaTresIguais(sorteados);
        somaGanhador(validaTresIguais);

    }

    public void sortear() {
        Random random = new Random();
        for (int i = 0; i < qtdSlots; i++) {
            int numeroAleatorio = random.nextInt(Slots.values().length);
            sorteados.add(Slots.values()[numeroAleatorio]);
        }
        output.imprimirMensagem("Slots Sorteados: " +sorteados);

    }

    public boolean validaTresIguais(ArrayList<Slots> sorteados) {
        int validador = 0;

        for (Slots slot : sorteados){
            if(slot.equals(sorteados.get(0))){
                validador++;
            }
        }

        if (validador == sorteados.size()){
            return true;
        }
        return false;
    }

    public void somaGanhador(boolean validador){
        int resultado =0;

        for (Slots slots: sorteados){
            resultado += slots.getValor();
        }

        if (validador){
            resultado = resultado*100;
        }
        this.soma = resultado;
        output.imprimirMensagem("A soma foi de: " +soma);
    }
}








