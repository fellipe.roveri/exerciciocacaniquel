package com.company;

public enum Slots {
    Banana(10),
    Framboesa(50),
    Moeda(100),
    sete(300);

    int valor;

    Slots(int valor) {
        this.valor = valor;
    }

    public int getValor(){
        return this.valor;
    }

}
